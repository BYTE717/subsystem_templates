public class PositionDelta{
    public class Pair<T, U> {         
        public final T t;
        public final U u;
        public Pair(T t, U u) {         
            this.t= t;
            this.u= u;
        }
    };

    public static Double getAngleToPose(Pose2d pose, Pose2d target){
        double dx = target.getX() - pose.getX();
        double dy = target.getY() - pose.getY();
        double angle = (Math.atan(dy / dx));
        double radians = pose.getRotation().getRadians();
        return radians - angle;
    }

    public static Double getDistance(Pose2d A, Pose2d B){
        double dx = A.getX() - B.getX(), dy = A.getY() - B.getY();
        return Math.sqrt((dx*dx) + (dy*dy));
    }

    public static void moveToPosition(Pose2d target, Double targetSpeed, boolean headOn=false){
        Pose2d pose = getPose();
        var drive = getDrivetrainInstance();

        Double dist = getDistance(pose, target);
        Double deltaAngle = getAngleToPose(pose, target);
        targetSpeed = Math.min(MAX_SPEED, Math.max(0, target));

        if(headOn){// head on = turn first and then go forward straight to target
            drive.setAngle(deltaAngle);
            drive.goForward(dist);
            return;
        } // otherwise, we need to match the angle while traveling
        // d = rt; t = d/r
        Double time = dist/targetSpeed;
        Double radsPerSecond = Math.min(MAX_ROTATION, Math.max(0, deltaAngle/time));

        // ill do the rest later, cuz how am i supposed to change coordinates on a line while also rotating
        
    }


    public static Pair<Double, Double> polarToRectangular(double r, double theta) {
        double x = r * Math.cos(Math.toRadians(theta));
        double y = r * Math.sin(Math.toRadians(theta));
        return new Pair<Double, Double>(x, y);
    }
    public static Pair<Double, Double> rectangularToPolar(double x, double y) {
        double r = Math.sqrt(x * x + y * y);
        double theta = Math.toDegrees(Math.atan2(y, x));
        return new Pair<Double, Double>(r, theta);
    }
}